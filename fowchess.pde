//==========================================
// fog of war - chess
//=========================================

// todo:
// - grafik / design / texte

//==================================================

// input:
// - drag yor own figures with the mouse
// - click on a figure to show the move-possibilities
// - ziehen ist nur auf markierte felder moeglich
// - push "space" to show the beaten figures
// - push "r" to show "die regeln"
// - winning is only possible by beating the king (chess/matt is not enough!)
// - bauer wird automatisch gegen eine dame ausgetauscht / bauer-umtausch

//==================================================

// cheats:
// - push "f" to remove the fog of war (ausgeschaltet!)

//===========================================

// define "hard" variables
int fieldwidth = 50;
int fieldcolor1 = 190;
int fieldcolor2 = 80;
int fieldbordercolor = 0;
int player1color = 255;
int player2color = 0;
color fieldmarkercolor = color (23, 3, 250);
color markedfieldcolor = color (250, 100, 10);
color fogofwarcolor = color (140, 140, 0);
color pfieldscolor = color (40, 250, 250);

// load images
PImage startbild, spielbrett, regeln, geschlagen, weiss, schwarz, sternweiss, sternschwarz;

// create figures
figure[] figures = new figure[32];

// create fog of war
boolean[][] fogofwar = new boolean[8][8];

// create possible-fields
boolean[][] pfields = new boolean[8][8];

// define "soft" variables
int i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13;
int drawcolor;
int markedfigure;
int phase = 0;
boolean mousereleased;
boolean figureselected;

//===============================================

void setup() {

  // create and open window
  size(401, 401);
  
  // load images
  startbild = loadImage("start.jpg");
  spielbrett = loadImage("brett.jpg");
  regeln = loadImage("regeln.jpg");
  geschlagen = loadImage("geschlagen.jpg");
  weiss = loadImage("weiss.jpg");
  schwarz = loadImage("schwarz.jpg");
  sternweiss = loadImage("sternweiss.gif");
  sternschwarz = loadImage("sternschwarz.gif");
  
}

//====================================================

void draw() {
  
  // clear screen
  background(0);
  
  // draw

  // draw intro  
  if (phase == 0){
    image (startbild, 0, 0);
    loadfigures();
  }
  
  // draw regeln at the beginning
  if (phase == 10){
    image (regeln, 0, 0);
  }
  
  // draw the displays, that shows who is on turn
  if (phase == 1 || phase == 4){
  
    if (phase == 1){
      image (weiss, 0, 0);
    }
  
    if (phase == 4){
      image (schwarz, 0, 0);
    }
  
  }
  
  // draw the field: the turn
  if (phase == 2 || phase == 5){
    drawfield(fieldwidth);
    drawfigures();
//    if ((keyPressed == true) && ((key == 'f') || (key == 'F'))) {
//    }
//    else {
      drawfogofwar();
//    }

    if (figureselected == true){
      drawpossiblefields();
    }
    
    drawmousefield();    
        
    if (figureselected == true){
      drawmarkedfield();    
    }

    if ((keyPressed == true) && (key == ' ')){
      drawbeatenfigures();
    }
    if ((keyPressed == true) && ((key == 'r') || (key == 'R'))){
      image (regeln, 0, 0);
    }

  }

  // draw the field: after the turn
  if (phase == 3 || phase == 6){
    drawfield(fieldwidth);
    drawfigures();
//    if ((keyPressed == true) && ((key == 'f') || (key == 'F'))) {
//    }
//    else {
      drawfogofwar();
//    }
    
    if ((keyPressed == true) && (key == ' ')){
      drawbeatenfigures();
    }
    if ((keyPressed == true) && ((key == 'r') || (key == 'R'))){
      image (regeln, 0, 0);
    }
    
  }

  // draw the winning-screen for player1
  if (phase == 7){
    drawfield(fieldwidth);
    drawfigures();
    drawwin(1);
  }
 
  // draw the winning-screen for player2
  if (phase == 8){
    drawfield(fieldwidth);
    drawfigures();
    drawwin(2);
  }

  // draw frame
  stroke (0);
  strokeWeight (1);
  noFill ();
  rect (0, 0, 400, 400);

}

void mousePressed(){


  
  // player 1
  if (phase == 2){
    if (figuretheremouse() == 1){
      markedfigure = mousefigure();
      figureselected = true;
    }
  }
  
// player 2
  if (phase == 5){
    if (figuretheremouse() == 2){
      markedfigure = mousefigure();
      figureselected = true;  
    }
  }  


  createpossiblefields(markedfigure);

}

void mouseReleased(){

  // this is needed to get only one mousereleased-signal each loop
  mousereleased = true;

  // intro
  if (mousereleased == true && phase == 0){  
    phase = 10;
    mousereleased = false;
    createfogofwar(1);
  }

  // regeln
  if (mousereleased == true && phase == 10){  
    phase = 1;
    mousereleased = false;
  }

  // display, that the 1st-player is on turn
  if (mousereleased == true && phase == 1){
    phase = 2;
    createfogofwar(1);
    mousereleased = false;
  }

  // 1st-players turn
  if (mousereleased == true && phase == 2 && figureselected == true && pfields[mouseoverx() - 1][mouseovery() - 1] == false){
    if (mouseoverx() != figures[markedfigure].xpos || mouseovery() != figures[markedfigure].ypos){
      int temp30 = mousefigure();
      // wenn versucht wird auf eigene figur zu ziehen
      if (figurethere(mouseoverx(), mouseovery()) == figures[markedfigure].player){
      }
      // wenn auf eine fremde figur gezogen wird
      if (figurethere(mouseoverx(), mouseovery()) == 2){
        figures[temp30].active = false;
        placebeatenfigures(temp30);
        figures[markedfigure].xpos = mouseoverx();
        figures[markedfigure].ypos = mouseovery();
        phase = 3;
        // wenn k�nig geschlagen
        if (figures[temp30].type == 'k'){
          phase = 7;
        }
      }
      // wenn auf ein leeres feld gezogen wird
      if (figurethere(mouseoverx(), mouseovery()) == 0){
        phase = 3;
        // Rochade weiss (1)
        if (figures[markedfigure].type == 'k'){ 
          // kurz
          if (figures[12].xpos == 5 && figures[12].ypos == 8 && figures[15].xpos == 8 && figures[15].ypos == 8){
            if (figurethere(6, 8) == 0 && figurethere(7, 8) == 0){
              figures[15].xpos = 6;
              figures[15].ypos = 8;
            }
          }
          // lang
          if (figures[12].xpos == 5 && figures[12].ypos == 8 && figures[8].xpos == 1 && figures[8].ypos == 8){
            if (figurethere(2, 8) == 0 && figurethere(3, 8) == 0 && figurethere(4, 8) == 0){
              figures[8].xpos = 4;
              figures[8].ypos = 8;
            }
          }
        }
        figures[markedfigure].xpos = mouseoverx();
        figures[markedfigure].ypos = mouseovery();
      }
      // Bauer-Umtausch
      if (figures[markedfigure].ypos == 1 && figures[markedfigure].type == 'b'){
        figures[markedfigure].type = 'd';
        figures[markedfigure].img = loadImage("Dw.gif"); 
      }
      // rest
      mousereleased = false;
      createfogofwar(1);
    }
  }

  // after the 1st-players turn
  if (mousereleased == true && phase == 3){
    phase = 4;
    mousereleased = false;
  }

  // display, that the 2nd-player is on turn  
  if (mousereleased == true && phase == 4){
    phase = 5;
    createfogofwar(2);
    mousereleased = false;
  }

  // 2nd-players turn
  if (mousereleased == true && phase == 5 && figureselected == true && pfields[mouseoverx() - 1][mouseovery() - 1] == false){
    if (mouseoverx() != figures[markedfigure].xpos || mouseovery() != figures[markedfigure].ypos){
      int temp30 = mousefigure();
      // wenn versucht wird auf eigene figur zu ziehen
      if (figurethere(mouseoverx(), mouseovery()) == figures[markedfigure].player){
      }
      // wenn auf eine fremde figur gezogen wird
      if (figurethere(mouseoverx(), mouseovery()) == 1){
        figures[temp30].active = false;
        placebeatenfigures(temp30);
        figures[markedfigure].xpos = mouseoverx();
        figures[markedfigure].ypos = mouseovery();
        phase = 6;
        // wenn k�nig geschlagen
        if (figures[temp30].type == 'k'){
          phase = 8;
        }
      }
      // wenn auf ein leeres feld gezogen wird
      if (figurethere(mouseoverx(), mouseovery()) == 0){
        phase = 6;
        // Rochade schwarz (2)
        if (figures[markedfigure].type == 'k'){ 
          // kurz
          if (figures[28].xpos == 5 && figures[28].ypos == 1 && figures[31].xpos == 8 && figures[31].ypos == 1){
            if (figurethere(6, 1) == 0 && figurethere(7, 1) == 0){
              figures[31].xpos = 6;
              figures[31].ypos = 1;
            }
          }
          // lang
          if (figures[28].xpos == 5 && figures[28].ypos == 1 && figures[24].xpos == 1 && figures[24].ypos == 1){
            if (figurethere(2, 1) == 0 && figurethere(3, 1) == 0 && figurethere(4, 1) == 0){
              figures[24].xpos = 4;
              figures[24].ypos = 1;
            }
          }
        }
        figures[markedfigure].xpos = mouseoverx();
        figures[markedfigure].ypos = mouseovery();
      }
      // Bauer-Umtausch
      if (figures[markedfigure].ypos == 8 && figures[markedfigure].type == 'b'){
        figures[markedfigure].type = 'd';
        figures[markedfigure].img = loadImage("Ds.gif"); 
      }
      // rest
      mousereleased = false;
      createfogofwar(2);
    }
  }

  // after the 2nd-players turn
  if (mousereleased == true && phase == 6){
    phase = 1;
    mousereleased = false;
  }

  // winning-screen player1
  if (mousereleased == true && phase == 7){
    phase = 0;
    mousereleased = false;
  }

  // winning-screen player2
  if (mousereleased == true && phase == 8){
    phase = 0;
    mousereleased = false;
  }

  // sonst
  figureselected = false;

}

//================================================

// draw the field, with variable fieldweight, at (0, 0) of the screen

void drawfield(int temp01){
  
  image (spielbrett, 0, 0);
  
}

// on which field is the mouse?

int mouseoverx(){
  return mouseX / fieldwidth + 1;
}

int mouseovery(){
  return mouseY / fieldwidth + 1;
}

// on which active figure is the mouse?

int mousefigure(){
  int temp05 = 0;
  for (i4 = 0; i4 <= 31; i4 += 1){
    if (figures[i4].xpos == mouseoverx() && figures[i4].ypos == mouseovery() && figures[i4].active == true){
      temp05 = i4;
    }
  }
  return temp05;
}

// is there an active figure below the mouse? and if yes, to which player does it belong?
// 0 - no figure
// 1 - player1
// 2 - player2

int figuretheremouse(){
  int temp06 = 0;
  for (i5 = 0; i5 <= 31; i5 += 1){
    if (figures[i5].xpos == mouseoverx() && figures[i5].ypos == mouseovery() && figures[i5].active == true){
      temp06 = figures[i5].player;
    }
  }
  return temp06;
}

// is there an active figure at a specific position? and if yes, to which player doeas it belong?

int figurethere(int temp10, int temp11){
  int temp06 = 0;
  for (i5 = 0; i5 <= 31; i5 += 1){
    if (figures[i5].xpos == temp10 && figures[i5].ypos == temp11 && figures[i5].active == true){
      temp06 = figures[i5].player;
    }
  }
  return temp06;
}


// is there an inactive figure at a specific position? and if yes, to which player doeas it belong?

int inactivefigurethere(int temp10, int temp11){
  int temp06 = 0;
  for (i5 = 0; i5 <= 31; i5 += 1){
    if (figures[i5].xpos == temp10 && figures[i5].ypos == temp11 && figures[i5].active == false){
      temp06 = figures[i5].player;
    }
  }
  return temp06;
}


// draw the marker for the mouse (where is the mouse?)

void drawmousefield(){

  int x1 = mouseoverx() * fieldwidth;
  int y1 = mouseovery() * fieldwidth;
  
  stroke (fieldmarkercolor);
  strokeWeight (3);
  noFill ();
  rect (x1 - fieldwidth, y1 - fieldwidth, fieldwidth, fieldwidth);

}

// draw figure-marker

void drawmarkedfield(){
  if (mousePressed == true){
    int x1 = figures[markedfigure].xpos * fieldwidth;
    int y1 = figures[markedfigure].ypos * fieldwidth;
    stroke (markedfieldcolor);
    strokeWeight (3);
    rect (x1 - fieldwidth, y1 - fieldwidth, fieldwidth, fieldwidth); 
  }
}

// draw each figure

void drawfigures(){
  for (i3 = 0; i3 <= 31; i3 += 1){
    figures[i3].drawfigure();
  }
}

// create fog of war, depending on the player

void createfogofwar (int temp08){

  // make everything unvisible
  for (i6 = 0; i6 <= 7; i6 += 1){
    for (i7 = 0; i7 <= 7; i7 += 1){
      fogofwar[i6][i7] = true;
    }
  }

  // make several fields visible
  for (i6 = 0; i6 <= 31; i6 += 1){
    
    if (figures[i6].player == temp08 && figures[i6].active == true){
      
      // make each field with own figures on it visible
      fogofwar[figures[i6].xpos - 1][figures[i6].ypos - 1] = false;

      // Bauer
      if (figures[i6].type == 'b'){
        // player 1
        if (figures[i6].player == 1){
          // eins nach vorne
          if (figures[i6].ypos > 1){
            fogofwar[figures[i6].xpos - 1][figures[i6].ypos - 1 - 1] = false;
          }
          // schr�g links & schr�g rechts
          if (figures[i6].xpos < 8 && figures[i6].ypos > 1){
            fogofwar[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 - 1] = false;
          }
          if (figures[i6].xpos > 1 && figures[i6].ypos > 1){
            fogofwar[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 - 1] = false;
          }
          // figur in der ersten reihe
          if (figures[i6].ypos == 7){
            if (figurethere(figures[i6].xpos, figures[i6].ypos - 1) == 0){
              fogofwar[figures[i6].xpos - 1][figures[i6].ypos - 1 - 2] = false;   
            }
          }
        }
        // player 2
        if (figures[i6].player == 2){
          // eins nach vorne
          if (figures[i6].ypos < 8){
            fogofwar[figures[i6].xpos - 1][figures[i6].ypos - 1 + 1] = false;
          }
          // schr�g links & schr�g rechts
          if (figures[i6].xpos < 8 && figures[i6].ypos < 8){
            fogofwar[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 + 1] = false;
          }
          if (figures[i6].xpos > 1 && figures[i6].ypos < 8){
            fogofwar[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 + 1] = false;
          }
          // figur in der ersten reihe
          if (figures[i6].ypos == 2){
            if (figurethere(figures[i6].xpos, figures[i6].ypos + 1) == 0){
              fogofwar[figures[i6].xpos - 1][figures[i6].ypos - 1 + 2] = false;   
            }
          }
        }
      }
      
      // Turm & Dame (halb)
      if (figures[i6].type == 't' || figures[i6].type == 'd'){
        int temp15; // abstand zum rand
        int temp16;
        boolean temp17;
        // nach oben
        temp15 = figures[i6].ypos - 1;
        temp16 = 1;
        temp17 = false;
        for (i8 = temp15; i8 > 0; i8 -= 1){
          if (temp17 == false){
            fogofwar[figures[i6].xpos - 1][figures[i6].ypos - 1 - temp16] = false;
          }
          if (figurethere(figures[i6].xpos, figures[i6].ypos - temp16) > 0){
            temp17 = true;
          }
          temp16 = temp16 + 1;
        }
        // nach unten
        temp15 = figures[i6].ypos + 1;
        temp17 = false;
        for (i8 = temp15; i8 < 9; i8 += 1){
          if (temp17 == false){
            fogofwar[figures[i6].xpos - 1][i8 - 1] = false;
          }
          if (figurethere(figures[i6].xpos, i8) > 0){
            temp17 = true;
          }
        }
        // nach links
        temp15 = figures[i6].xpos - 1;
        temp16 = 1;
        temp17 = false;
        for (i8 = temp15; i8 > 0; i8 -= 1){
          if (temp17 == false){
            fogofwar[figures[i6].xpos - 1 - temp16][figures[i6].ypos - 1] = false;
          }
          if (figurethere(figures[i6].xpos - temp16, figures[i6].ypos) > 0){
            temp17 = true;
          }
          temp16 = temp16 + 1;
        }  
        // nach rechts
        temp15 = figures[i6].xpos + 1;
        temp17 = false;
        for (i8 = temp15; i8 < 9; i8 += 1){
          if (temp17 == false){
            fogofwar[i8 - 1][figures[i6].ypos - 1] = false;
          }
          if (figurethere(i8, figures[i6].ypos) > 0){
            temp17 = true;
          }
        }
      } 

      // Springer (im uhrzeigersinn)
      if (figures[i6].type == 's'){
        // rechts
        if (figures[i6].xpos < 8 && figures[i6].ypos > 2){
          fogofwar[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 - 2] = false;
        }
        if (figures[i6].xpos < 7 && figures[i6].ypos > 1){
          fogofwar[figures[i6].xpos - 1 + 2][figures[i6].ypos - 1 - 1] = false;
        }
        if (figures[i6].xpos < 7 && figures[i6].ypos < 8){
          fogofwar[figures[i6].xpos - 1 + 2][figures[i6].ypos - 1 + 1] = false;
        }
        if (figures[i6].xpos < 8 && figures[i6].ypos < 7){
          fogofwar[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 + 2] = false;
        }
        // links
        if (figures[i6].xpos > 1 && figures[i6].ypos < 7){
          fogofwar[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 + 2] = false;
        }
        if (figures[i6].xpos > 2 && figures[i6].ypos < 8){
          fogofwar[figures[i6].xpos - 1 - 2][figures[i6].ypos - 1 + 1] = false;
        }
        if (figures[i6].xpos > 2 && figures[i6].ypos > 1){
          fogofwar[figures[i6].xpos - 1 - 2][figures[i6].ypos - 1 - 1] = false;
        }
        if (figures[i6].xpos > 1 && figures[i6].ypos > 2){
          fogofwar[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 - 2] = false;
        }
      } 

      // L�ufer & Dame (halb)
      if (figures[i6].type == 'l' || figures[i6].type == 'd'){
        int temp15;
        int temp16;
        boolean temp17; 
        boolean temp18;
        // nach rechts
        temp15 = figures[i6].xpos + 1;
        temp17 = false;
        temp18 = false;
        for (i8 = temp15; i8 < 9; i8 += 1){
          // nach unten
          temp16 = figures[i6].ypos + 1;
          for (i9 = temp16; i9 < 9; i9 += 1){
            if (i8 - figures[i6].xpos == i9 - figures[i6].ypos){
              if (temp17 == false){
                fogofwar[i8 - 1][i9 - 1] = false;
              }
              if (figurethere(i8, i9) > 0){
                temp17 = true;
              }
            }
          } 
          // nach oben
          temp16 = figures[i6].ypos - 1;
          for (i9 = temp16; i9 > 0; i9 -= 1){
            if (i8 - figures[i6].xpos == figures[i6].ypos - i9){
              if (temp18 == false){
                fogofwar[i8 - 1][i9 - 1] = false;
              }
              if (figurethere(i8, i9) > 0){
                temp18 = true;
              }
            }
          }
        }
        // nach links
        temp15 = figures[i6].xpos - 1;
        temp17 = false;
        temp18 = false;
        for (i8 = temp15; i8 > 0; i8 -= 1){
          // nach unten
          temp16 = figures[i6].ypos + 1;
          for (i9 = temp16; i9 < 9; i9 += 1){
            if (figures[i6].xpos - i8 == i9 - figures[i6].ypos){
              if (temp17 == false){
                fogofwar[i8 - 1][i9 - 1] = false;
              }
              if (figurethere(i8, i9) > 0){
                temp17 = true;
              }
            }
          }
          // nach oben
          temp16 = figures[i6].ypos - 1;
          for (i9 = temp16; i9 > 0; i9 -= 1){
            if (figures[i6].xpos - i8 == figures[i6].ypos - i9){
              if (temp18 == false){
                fogofwar[i8 - 1][i9 - 1] = false;
              }
              if (figurethere(i8, i9) > 0){
                temp18 = true;
              }
            }
          }
        }        
      } 

      // Dame: siehe Turm & L�ufer

      // K�nig
      if (figures[i6].type == 'k'){
        // gerade
        if (figures[i6].xpos < 8){
          fogofwar[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1] = false;
        }
        if (figures[i6].xpos > 1){
          fogofwar[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1] = false;
        }        
        if (figures[i6].ypos < 8){
          fogofwar[figures[i6].xpos - 1][figures[i6].ypos - 1 + 1] = false;
        }
        if (figures[i6].ypos > 1){
          fogofwar[figures[i6].xpos - 1][figures[i6].ypos - 1 - 1] = false;
        }        
        // schr�g
        if (figures[i6].xpos < 8 && figures[i6].ypos < 8){
          fogofwar[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 + 1] = false;
        }
        if (figures[i6].xpos > 1 && figures[i6].ypos > 1){
          fogofwar[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 - 1] = false;
        }
        if (figures[i6].xpos < 8 && figures[i6].ypos > 1){
          fogofwar[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 - 1] = false;
        }
        if (figures[i6].xpos > 1 && figures[i6].ypos < 8){
          fogofwar[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 + 1] = false;
        } 
      } 
    }
  }
}

// draw the fog of war

void drawfogofwar(){

  fill (fogofwarcolor);
  strokeWeight (0);
  stroke (0);
  
  for (i6 = 0; i6 <= 7; i6 += 1){
    for (i7 = 0; i7 <= 7; i7 += 1){
      if (fogofwar[i6][i7] == true){
        rect (i6 * fieldwidth, i7 * fieldwidth + 1, fieldwidth, fieldwidth);
      }
    }
  }
}

// create the possible fields for one figure

void createpossiblefields (int i6){

  // make every field not-possible
  for (i9 = 0; i9 <= 7; i9 += 1){
    for (i7 = 0; i7 <= 7; i7 += 1){
      pfields[i9][i7] = true;
    }
  }

  if (figures[i6].active == true){

      // Bauer
      if (figures[i6].type == 'b'){
        // player 1
        if (figures[i6].player == 1){
          // eins nach vorne
          if (figurethere(figures[i6].xpos, figures[i6].ypos - 1) == 0){
            pfields[figures[i6].xpos - 1][figures[i6].ypos - 1 - 1] = false;
            // figur in der ersten reihe
            if (figures[i6].ypos == 7){
              if (figurethere(figures[i6].xpos, figures[i6].ypos - 2) == 0){
                pfields[figures[i6].xpos - 1][figures[i6].ypos - 1 - 2] = false;   
              }
            }
          }
          // schr�g links & schr�g rechts
          if (figures[i6].xpos < 8 && figures[i6].ypos > 1){
            if (figurethere(figures[i6].xpos + 1, figures[i6].ypos - 1) == 2){
              pfields[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 - 1] = false;
            }
          }
          if (figures[i6].xpos > 1 && figures[i6].ypos > 1){
            if (figurethere(figures[i6].xpos - 1, figures[i6].ypos - 1) == 2){
              pfields[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 - 1] = false;
            }
          }
        }
        // player 2
        if (figures[i6].player == 2){
          // eins nach vorne
          if (figurethere(figures[i6].xpos, figures[i6].ypos + 1) == 0){
            pfields[figures[i6].xpos - 1][figures[i6].ypos - 1 + 1] = false;
            // figur in der ersten reihe
            if (figures[i6].ypos == 2){
              if (figurethere(figures[i6].xpos, figures[i6].ypos + 2) == 0){
                pfields[figures[i6].xpos - 1][figures[i6].ypos - 1 + 2] = false;   
              }
            }
          }
          // schr�g links & schr�g rechts
          if (figures[i6].xpos < 8 && figures[i6].ypos < 8){
            if (figurethere(figures[i6].xpos + 1, figures[i6].ypos + 1) == 1){
              pfields[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 + 1] = false;
            }
          }
          if (figures[i6].xpos > 1 && figures[i6].ypos < 8){
            if (figurethere(figures[i6].xpos - 1, figures[i6].ypos + 1) == 1){
              pfields[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 + 1] = false;
            }
          }
        }
      } 

      // Turm & Dame (halb)
      if (figures[i6].type == 't' || figures[i6].type == 'd'){
        int temp15; // abstand zum rand
        int temp16;
        boolean temp17;
        // nach oben
        temp15 = figures[i6].ypos - 1;
        temp16 = 1;
        temp17 = false;
        for (i8 = temp15; i8 > 0; i8 -= 1){
          if (temp17 == false){
            if (figurethere(figures[i6].xpos, figures[i6].ypos - temp16) != figures[i6].player){
              pfields[figures[i6].xpos - 1][figures[i6].ypos - 1 - temp16] = false;
            }
          }
          if (figurethere(figures[i6].xpos, figures[i6].ypos - temp16) > 0){
            temp17 = true;
          }
          temp16 = temp16 + 1;
        }
        // nach unten
        temp15 = figures[i6].ypos + 1;
        temp17 = false;
        for (i8 = temp15; i8 < 9; i8 += 1){
          if (temp17 == false){
            if (figurethere(figures[i6].xpos, i8) != figures[i6].player){
              pfields[figures[i6].xpos - 1][i8 - 1] = false;
            }  
          }
          if (figurethere(figures[i6].xpos, i8) > 0){
            temp17 = true;
          }
        }
        // nach links
        temp15 = figures[i6].xpos - 1;
        temp16 = 1;
        temp17 = false;
        for (i8 = temp15; i8 > 0; i8 -= 1){
          if (temp17 == false){
            if (figurethere(figures[i6].xpos - temp16, figures[i6].ypos) != figures[i6].player){
              pfields[figures[i6].xpos - 1 - temp16][figures[i6].ypos - 1] = false;
            }
          }
          if (figurethere(figures[i6].xpos - temp16, figures[i6].ypos) > 0){
            temp17 = true;
          }
          temp16 = temp16 + 1;
        }  
        // nach rechts
        temp15 = figures[i6].xpos + 1;
        temp17 = false;
        for (i8 = temp15; i8 < 9; i8 += 1){
          if (temp17 == false){
            if (figurethere(i8, figures[i6].ypos) != figures[i6].player){
              pfields[i8 - 1][figures[i6].ypos - 1] = false;
            }
          }
          if (figurethere(i8, figures[i6].ypos) > 0){
            temp17 = true;
          }
        }
      } 

      // Springer (im uhrzeigersinn)
      if (figures[i6].type == 's'){
        // rechts
        if (figures[i6].xpos < 8 && figures[i6].ypos > 2){
          if (figurethere(figures[i6].xpos + 1, figures[i6].ypos - 2) != figures[i6].player){
            pfields[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 - 2] = false;
          }
        }
        if (figures[i6].xpos < 7 && figures[i6].ypos > 1){
          if (figurethere(figures[i6].xpos + 2, figures[i6].ypos - 1) != figures[i6].player){
            pfields[figures[i6].xpos - 1 + 2][figures[i6].ypos - 1 - 1] = false;
          }
        }
        if (figures[i6].xpos < 7 && figures[i6].ypos < 8){
          if (figurethere(figures[i6].xpos + 2, figures[i6].ypos + 1) != figures[i6].player){
            pfields[figures[i6].xpos - 1 + 2][figures[i6].ypos - 1 + 1] = false;
          }
        }
        if (figures[i6].xpos < 8 && figures[i6].ypos < 7){
          if (figurethere(figures[i6].xpos + 1, figures[i6].ypos + 2) != figures[i6].player){
            pfields[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 + 2] = false;
          }
        }
        // links
        if (figures[i6].xpos > 1 && figures[i6].ypos < 7){
          if (figurethere(figures[i6].xpos - 1, figures[i6].ypos + 2) != figures[i6].player){
            pfields[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 + 2] = false;
          }
        }
        if (figures[i6].xpos > 2 && figures[i6].ypos < 8){
          if (figurethere(figures[i6].xpos - 2, figures[i6].ypos + 1) != figures[i6].player){
            pfields[figures[i6].xpos - 1 - 2][figures[i6].ypos - 1 + 1] = false;
          }
        }
        if (figures[i6].xpos > 2 && figures[i6].ypos > 1){
          if (figurethere(figures[i6].xpos - 2, figures[i6].ypos - 1) != figures[i6].player){
            pfields[figures[i6].xpos - 1 - 2][figures[i6].ypos - 1 - 1] = false;
          }
        }
        if (figures[i6].xpos > 1 && figures[i6].ypos > 2){
          if (figurethere(figures[i6].xpos - 1, figures[i6].ypos - 2) != figures[i6].player){
            pfields[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 - 2] = false;
          }
        }
      } 

      // L�ufer & Dame (halb)
      if (figures[i6].type == 'l' || figures[i6].type == 'd'){
        int temp15;
        int temp16;
        boolean temp17; 
        boolean temp18;
        // nach rechts
        temp15 = figures[i6].xpos + 1;
        temp17 = false;
        temp18 = false;
        for (i8 = temp15; i8 < 9; i8 += 1){
          // nach unten
          temp16 = figures[i6].ypos + 1;
          for (i9 = temp16; i9 < 9; i9 += 1){
            if (i8 - figures[i6].xpos == i9 - figures[i6].ypos){
              if (temp17 == false){
                if (figurethere(i8, i9) != figures[i6].player){
                  pfields[i8 - 1][i9 - 1] = false;
                }
              }
              if (figurethere(i8, i9) > 0){
                temp17 = true;
              }
            }
          } 
          // nach oben
          temp16 = figures[i6].ypos - 1;
          for (i9 = temp16; i9 > 0; i9 -= 1){
            if (i8 - figures[i6].xpos == figures[i6].ypos - i9){
              if (temp18 == false){
                if (figurethere(i8, i9) != figures[i6].player){
                  pfields[i8 - 1][i9 - 1] = false;
                }
              }
              if (figurethere(i8, i9) > 0){
                temp18 = true;
              }
            }
          }
        }
        // nach links
        temp15 = figures[i6].xpos - 1;
        temp17 = false;
        temp18 = false;
        for (i8 = temp15; i8 > 0; i8 -= 1){
          // nach unten
          temp16 = figures[i6].ypos + 1;
          for (i9 = temp16; i9 < 9; i9 += 1){
            if (figures[i6].xpos - i8 == i9 - figures[i6].ypos){
              if (temp17 == false){
                if (figurethere(i8, i9) != figures[i6].player){
                  pfields[i8 - 1][i9 - 1] = false;
                }
              }
              if (figurethere(i8, i9) > 0){
                temp17 = true;
              }
            }
          }
          // nach oben
          temp16 = figures[i6].ypos - 1;
          for (i9 = temp16; i9 > 0; i9 -= 1){
            if (figures[i6].xpos - i8 == figures[i6].ypos - i9){
              if (temp18 == false){
                if (figurethere(i8, i9) != figures[i6].player){
                  pfields[i8 - 1][i9 - 1] = false;
                }
              }
              if (figurethere(i8, i9) > 0){
                temp18 = true;
              }
            }
          }
        }        
      } 

      // Dame: siehe Turm & L�ufer

      // K�nig
      if (figures[i6].type == 'k'){
        // gerade
        if (figures[i6].xpos < 8){
          if (figurethere(figures[i6].xpos + 1, figures[i6].ypos) != figures[i6].player){          
            pfields[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1] = false;
          }
        }
        if (figures[i6].xpos > 1){
          if (figurethere(figures[i6].xpos - 1, figures[i6].ypos) != figures[i6].player){
            pfields[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1] = false;
          }
        }        
        if (figures[i6].ypos < 8){
          if (figurethere(figures[i6].xpos, figures[i6].ypos + 1) != figures[i6].player){
            pfields[figures[i6].xpos - 1][figures[i6].ypos - 1 + 1] = false;
          }
        }
        if (figures[i6].ypos > 1){
          if (figurethere(figures[i6].xpos, figures[i6].ypos - 1) != figures[i6].player){
            pfields[figures[i6].xpos - 1][figures[i6].ypos - 1 - 1] = false;
          }
        }        
        // schr�g
        if (figures[i6].xpos < 8 && figures[i6].ypos < 8){
          if (figurethere(figures[i6].xpos + 1, figures[i6].ypos + 1) != figures[i6].player){
            pfields[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 + 1] = false;
          }
        }
        if (figures[i6].xpos > 1 && figures[i6].ypos > 1){
          if (figurethere(figures[i6].xpos - 1, figures[i6].ypos - 1) != figures[i6].player){
            pfields[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 - 1] = false;
          }
        }
        if (figures[i6].xpos < 8 && figures[i6].ypos > 1){
          if (figurethere(figures[i6].xpos + 1, figures[i6].ypos - 1) != figures[i6].player){
            pfields[figures[i6].xpos - 1 + 1][figures[i6].ypos - 1 - 1] = false;
          }
        }
        if (figures[i6].xpos > 1 && figures[i6].ypos < 8){
          if (figurethere(figures[i6].xpos - 1, figures[i6].ypos + 1) != figures[i6].player){
            pfields[figures[i6].xpos - 1 - 1][figures[i6].ypos - 1 + 1] = false;
          }
        }
        // Rochade weiss (1)
        if (figures[markedfigure].player == 1){
          // kurz
          if (figures[12].xpos == 5 && figures[12].ypos == 8 && figures[15].xpos == 8 && figures[15].ypos == 8){
            if (figurethere(6, 8) == 0 && figurethere(7, 8) == 0){
              pfields[6][7] = false;
            }
          }
          // lang
          if (figures[12].xpos == 5 && figures[12].ypos == 8 && figures[8].xpos == 1 && figures[8].ypos == 8){
            if (figurethere(2, 8) == 0 && figurethere(3, 8) == 0 && figurethere(4, 8) == 0){
              pfields[2][7] = false;
            }
          }
        }
        // Rochade schwarz (2)
        if (figures[markedfigure].player == 2){
          // kurz
          if (figures[28].xpos == 5 && figures[28].ypos == 1 && figures[31].xpos == 8 && figures[31].ypos == 1){
            if (figurethere(6, 1) == 0 && figurethere(7, 1) == 0){
              pfields[6][0] = false;
            }
          }
          // lang
          if (figures[28].xpos == 5 && figures[28].ypos == 1 && figures[24].xpos == 1 && figures[24].ypos == 1){
            if (figurethere(2, 1) == 0 && figurethere(3, 1) == 0 && figurethere(4, 1) == 0){
              pfields[2][0] = false;
            }
          }
        }
    }
  }
}

// draw the possible fields

void drawpossiblefields(){

  noFill ();
  strokeWeight (3);
  stroke (pfieldscolor);
  
  for (i6 = 0; i6 <= 7; i6 += 1){
    for (i7 = 0; i7 <= 7; i7 += 1){
      if (pfields[i6][i7] == false){
        rect (i6 * fieldwidth, i7 * fieldwidth, fieldwidth, fieldwidth);
      }
    }
  }
}

// place all beaten figures

void placebeatenfigures(int i11){
  // die einzelnen figuren werden durchgereicht bis zum ende der schleife!
  for (i12 = 8; i12 >= 3; i12 -= 1){
    for (i13 = 8; i13 >= 1; i13 -= 1){
      if (inactivefigurethere(i13, i12) == 0){
        figures[i11].xpos = i13;
        figures[i11].ypos = i12;
      } 
    } 
  }
}

// draw all inactive figures

void drawbeatenfigures(){

  // hintergrund
  image (geschlagen, 0, 0);
    
  for (i10 = 0; i10 <= 31; i10 += 1){
    
    if (figures[i10].active == false){      
      image (figures[i10].img, figures[i10].xpos * fieldwidth - fieldwidth + 3, figures[i10].ypos * fieldwidth - fieldwidth + 3);
    }
    
  }
}

// draw winning

void drawwin(int temp40){
  
  if (temp40 == 1){
    image (sternweiss, mouseX - 50, mouseY - 45);
  }
  if (temp40 == 2){
    image (sternschwarz, mouseX - 40, mouseY - 45);
  }  
  
}

// load figure-start-positions

void loadfigures(){

  // define white figures
  figures[0] = new figure();
  figures[0].xpos = 1;
  figures[0].ypos = 7;
  figures[0].player = 1;
  figures[0].type = 'b';
  figures[0].active = true;
  figures[0].img = loadImage("Bw.gif");

  figures[1] = new figure();
  figures[1].xpos = 2;
  figures[1].ypos = 7;
  figures[1].player = 1;
  figures[1].type = 'b';
  figures[1].active = true;
  figures[1].img = loadImage("Bw.gif");

  figures[2] = new figure();
  figures[2].xpos = 3;
  figures[2].ypos = 7;
  figures[2].player = 1;
  figures[2].type = 'b';
  figures[2].active = true;
  figures[2].img = loadImage("Bw.gif");

  figures[3] = new figure();
  figures[3].xpos = 4;
  figures[3].ypos = 7;
  figures[3].player = 1;
  figures[3].type = 'b';
  figures[3].active = true;
  figures[3].img = loadImage("Bw.gif");

  figures[4] = new figure();
  figures[4].xpos = 5;
  figures[4].ypos = 7;
  figures[4].player = 1;
  figures[4].type = 'b';
  figures[4].active = true;
  figures[4].img = loadImage("Bw.gif");

  figures[5] = new figure();
  figures[5].xpos = 6;
  figures[5].ypos = 7;
  figures[5].player = 1;
  figures[5].type = 'b';
  figures[5].active = true;
  figures[5].img = loadImage("Bw.gif");

  figures[6] = new figure();
  figures[6].xpos = 7;
  figures[6].ypos = 7;
  figures[6].player = 1;
  figures[6].type = 'b';
  figures[6].active = true;
  figures[6].img = loadImage("Bw.gif");

  figures[7] = new figure();
  figures[7].xpos = 8;
  figures[7].ypos = 7;
  figures[7].player = 1;
  figures[7].type = 'b';
  figures[7].active = true;
  figures[7].img = loadImage("Bw.gif");
  
  figures[8] = new figure();
  figures[8].xpos = 1;
  figures[8].ypos = 8;
  figures[8].player = 1;
  figures[8].type = 't';
  figures[8].active = true;
  figures[8].img = loadImage("Tw.gif");

  figures[9] = new figure();
  figures[9].xpos = 2;
  figures[9].ypos = 8;
  figures[9].player = 1;
  figures[9].type = 's';
  figures[9].active = true;
  figures[9].img = loadImage("Sw.gif");

  figures[10] = new figure();
  figures[10].xpos = 3;
  figures[10].ypos = 8;
  figures[10].player = 1;
  figures[10].type = 'l';
  figures[10].active = true;
  figures[10].img = loadImage("Lw.gif");

  figures[11] = new figure();
  figures[11].xpos = 4;
  figures[11].ypos = 8;
  figures[11].player = 1;
  figures[11].type = 'd';
  figures[11].active = true;
  figures[11].img = loadImage("Dw.gif");

  figures[12] = new figure();
  figures[12].xpos = 5;
  figures[12].ypos = 8;
  figures[12].player = 1;
  figures[12].type = 'k';
  figures[12].active = true;
  figures[12].img = loadImage("Kw.gif");

  figures[13] = new figure();
  figures[13].xpos = 6;
  figures[13].ypos = 8;
  figures[13].player = 1;
  figures[13].type = 'l';
  figures[13].active = true;
  figures[13].img = loadImage("Lw.gif");

  figures[14] = new figure();
  figures[14].xpos = 7;
  figures[14].ypos = 8;
  figures[14].player = 1;
  figures[14].type = 's';
  figures[14].active = true;
  figures[14].img = loadImage("Sw.gif");

  figures[15] = new figure();
  figures[15].xpos = 8;
  figures[15].ypos = 8;
  figures[15].player = 1;
  figures[15].type = 't';
  figures[15].active = true;
  figures[15].img = loadImage("Tw.gif");
  
  // define black figures
  figures[16] = new figure();
  figures[16].xpos = 1;
  figures[16].ypos = 2;
  figures[16].player = 2;
  figures[16].type = 'b';
  figures[16].active = true;
  figures[16].img = loadImage("Bs.gif");

  figures[17] = new figure();
  figures[17].xpos = 2;
  figures[17].ypos = 2;
  figures[17].player = 2;
  figures[17].type = 'b';
  figures[17].active = true;
  figures[17].img = loadImage("Bs.gif");

  figures[18] = new figure();
  figures[18].xpos = 3;
  figures[18].ypos = 2;
  figures[18].player = 2;
  figures[18].type = 'b';
  figures[18].active = true;
  figures[18].img = loadImage("Bs.gif");

  figures[19] = new figure();
  figures[19].xpos = 4;
  figures[19].ypos = 2;
  figures[19].player = 2;
  figures[19].type = 'b';
  figures[19].active = true;
  figures[19].img = loadImage("Bs.gif");

  figures[20] = new figure();
  figures[20].xpos = 5;
  figures[20].ypos = 2;
  figures[20].player = 2;
  figures[20].type = 'b';
  figures[20].active = true;
  figures[20].img = loadImage("Bs.gif");

  figures[21] = new figure();
  figures[21].xpos = 6;
  figures[21].ypos = 2;
  figures[21].player = 2;
  figures[21].type = 'b';
  figures[21].active = true;
  figures[21].img = loadImage("Bs.gif");

  figures[22] = new figure();
  figures[22].xpos = 7;
  figures[22].ypos = 2;
  figures[22].player = 2;
  figures[22].type = 'b';
  figures[22].active = true;
  figures[22].img = loadImage("Bs.gif");

  figures[23] = new figure();
  figures[23].xpos = 8;
  figures[23].ypos = 2;
  figures[23].player = 2;
  figures[23].type = 'b';
  figures[23].active = true;
  figures[23].img = loadImage("Bs.gif");
  
  figures[24] = new figure();
  figures[24].xpos = 1;
  figures[24].ypos = 1;
  figures[24].player = 2;
  figures[24].type = 't';
  figures[24].active = true;
  figures[24].img = loadImage("Ts.gif");

  figures[25] = new figure();
  figures[25].xpos = 2;
  figures[25].ypos = 1;
  figures[25].player = 2;
  figures[25].type = 's';
  figures[25].active = true;
  figures[25].img = loadImage("Ss.gif");

  figures[26] = new figure();
  figures[26].xpos = 3;
  figures[26].ypos = 1;
  figures[26].player = 2;
  figures[26].type = 'l';
  figures[26].active = true;
  figures[26].img = loadImage("Ls.gif");

  figures[27] = new figure();
  figures[27].xpos = 4;
  figures[27].ypos = 1;
  figures[27].player = 2;
  figures[27].type = 'd';
  figures[27].active = true;
  figures[27].img = loadImage("Ds.gif");

  figures[28] = new figure();
  figures[28].xpos = 5;
  figures[28].ypos = 1;
  figures[28].player = 2;
  figures[28].type = 'k';
  figures[28].active = true;
  figures[28].img = loadImage("Ks.gif");

  figures[29] = new figure();
  figures[29].xpos = 6;
  figures[29].ypos = 1;
  figures[29].player = 2;
  figures[29].type = 'l';
  figures[29].active = true;
  figures[29].img = loadImage("Ls.gif");

  figures[30] = new figure();
  figures[30].xpos = 7;
  figures[30].ypos = 1;
  figures[30].player = 2;
  figures[30].type = 's';
  figures[30].active = true;
  figures[30].img = loadImage("Ss.gif");

  figures[31] = new figure();
  figures[31].xpos = 8;
  figures[31].ypos = 1;
  figures[31].player = 2;
  figures[31].type = 't';
  figures[31].active = true;
  figures[31].img = loadImage("Ts.gif");

}

//=================================================

class figure {

  int player;
  char type; 
  int xpos, ypos;
  PImage img;
  boolean active;
  
  void drawfigure(){
    if (active == true){
      image (img, xpos * fieldwidth - fieldwidth + 3, ypos * fieldwidth - fieldwidth + 3);
    }  
  }
}
