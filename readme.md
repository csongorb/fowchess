# Fog Of War Chess

A local multiplayer (hot-seat) fog of war chess.

Releases under [https://bitbucket.org/csongorb/fowchess/downloads/](https://bitbucket.org/csongorb/fowchess/downloads/)

## Made With

- [Processing](https://processing.org/)
- assets for the chess pieces
	- I'm really sorry, but I have no clue where I have this from (see below, that was a long time ago)

## Journal

### 2020-03-12

Re-discovering an old project of mine. And saving it for... whatever. Maybe it's just nostalgia.

Primary made with the purpose of learning to code. This is basically my first digital project I made completely on my own.

Supposedly developed with Processing 0148 - 0155 in 2008, thats a long time ago. Fur sure developed without git, so this here is just the faked history of the game, rebuild based on the files if have.

There are mainly two reasons to do it now:

- students are asking me to be more open with my personal projects, so here we go
- Pippin Barr doing a Fog of War Chess version on his own
	- [https://twitter.com/pippinbarr/status/1237812501288497159](https://twitter.com/pippinbarr/status/1237812501288497159)
	- I was already reminded on my old fow-chess when he released [CHESSES](https://pippinbarr.github.io/chesses/), but now I had to act, just for my own sake

## Log / Version-History

### Release 1.0 (supposedly 2008-11-09)

- originally released on the website of a dentist I know (not online anymore)

### Todos / Bugs / Ideas

## Credits

- **csongor baranyai**  
	- csongorb (at) gmail (dot) com  
	- [www.csongorb.com](http://www.csongorb.com)
- **tine tillmann**

## License

[![Creative Commons License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/)
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).

All other media (images, software, etc.) remain the property of their copyright holders.
